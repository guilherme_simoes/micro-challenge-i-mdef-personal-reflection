# Micro Challenge I MDEF Personal Reflection

Personal/Individual Reflection on the Micro Challenge I.

## **Initial Questions:**

#### 1. What Can You Do With a Laser Cutter and a CNC Machine?
#### 2. What Are Their Potential Real-World Applications?
#### 3. How To Simply/Easily Make a Fun and Interactive Board Game from Scratch?
#### 4. What is the Best CAD (Computer-Aided Design) Software to Make this Project?
#### 5. How Can Me and Mark Mash Up Our Interests & Research Areas Together (Food and Water)?

## **Facts: An Objective Account of What Happened**

#### First of all, me and Mark began doing the challenge relatively late. We teamed up half a week before the challenge final presentation. Now, taking this into account, we had to build up something fast and efficiently, since time was running out. We decided to do a "easy and simple board game". Yes, I ironically quoted the passage, because building a board game, like we later would find out, is anything but easy and simple. And, the hardest part is not even designing and making all the different components, but coming up with a reasonable and feasable back story for the game itself in such a short amount of time. It's very time-consuming. Therefore, we start conceptualizing a new type of game, one that would combine together the topics of food and water, with an educational prime goal/objective too. And this is how we ended up with a card game measuring the water footprint of many different foodstuffs, specially animal-derived products, to increase awareness and knowledge of this utmost critical problem, from an environmental perspective. Since this is a trivia game, this would be done in a fun, interactive and relaxed way, unlike the approach taken by the lovely darling Greta Thunberg, for example. We had the concept of the game ready, now it was time to make it!! After all the sketching, designing and prototyping, we laser cut the components on a not so thick cardboard sheet and printed out all of the graphic elements of the game on nearby printing shop. We still managed to have a few failures & setbacks, but in the end, against all odds, we successfully completed the challenge.  

## **Feelings: The Emotional Reactions to the Situation**

#### I felt kind oh lonely and hepless in the beginning, sice I couldn't find a "business partner'. But I can say that I'm very glad and happy that I was able to pair up with Mark. Besides being a good partner, he's a very talented designer and maker, so, coupled with my intellectual and creative powerhouse, we were able to pull this off. I was also very impressed with how fast I learned new skills in only such a short few days, hands-on learning truly is the best way to learn something new.

## **Findings: The Concrete Learning You Can Take From the Situation**

#### The biggest lesson from me was that making things is not as hard and complicated as I thought. That's a great personal victory, specially coming from a business/entrepreneurship background like me. Besides that, having faith and confidence always gets things done...

## **Future: Structuring Your Learning Such that You Can Use it in the Future**

#### Continue studying the Fab Academy content, after the Master specially. Also, getting my hands "dirty" more often, in order to become a maker, so that learning becomes more effective and solid.

